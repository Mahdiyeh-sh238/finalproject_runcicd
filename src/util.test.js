const puppeteer = require('puppeteer');
const { generateText,validateAndGenerate } = require('./util');

test('output title and price and number', () => {
    const text = generateText('Book', 29, 50);
    expect(text).toBe('Book 29 50');
})

test('check and generate input', () => {
    const text = validateAndGenerate('Book', 79,50);
    expect(text).toBe('Book 79 50');
});



