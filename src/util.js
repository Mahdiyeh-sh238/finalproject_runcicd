const generateText = (title, price,number) => {
    return `${title} ${price} ${number}`
}

exports.createElement = (type, text, className) => {
    const newElm = document.createElement(type);
    newElm.classList.add(className);
    newElm.textContent = text;
    return newElm;
}

const validateInput = (text, notEmpty, isNumber) => {
    if (!text) {
        return false;
    }
    if (notEmpty && text.trim().length === 0) {
        return false;
    }
    if (isNumber & text === NaN) {
        return false;
    }
    return true;
}

exports.validateAndGenerate = (title, price,number) => {
    if (!validateInput(title, true, false) || !validateInput(price, false, true) || !validateInput(number, false, true)) {
        return false;
    }
    return generateText(title, price,number);
}

exports.generateText = generateText;
exports.validateInput = validateInput;

