import 'bootstrap/dist/css/bootstrap.css'
import '../style/bootstrap.css'
const { validateAndGenerate, createElement } = require('./util');

const initApp = () => {
    const btnNewProduct = document.querySelector("#add-product");
    btnNewProduct.addEventListener('click', addProduct)
}

const addProduct = () => {
    const productTitle = document.querySelector('#title');
    const productPrice = document.querySelector('#price');
    const productNumber = document.querySelector('#number');
    const products = document.querySelector('.products');
    const titleValue = productTitle.value;
    const priceValue = productPrice.value;
    const numberValue=productNumber.value;
    const output = validateAndGenerate(titleValue, priceValue,numberValue);
    if (!output) return;
    const productEl = createElement('li', output, 'product-list');
    products.appendChild(productEl);
}
initApp();
const port = 3005
const host = '127.0.0.1'
server.listen(port, host)
console.log(`Listening at http://${host}:${port}`)
