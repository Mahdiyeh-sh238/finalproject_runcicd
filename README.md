# finalProject_RunCiCd



## راه اندازی گیت لب راننر
 برای راه اندازی گیت لب راننر روی یک سرور جداگانه، باید ابتدا Docker را بر روی سرور نصب کنیم. برای نصب Docker طبق داکیومنت داکر پیش میرویم.
پس دستورات زیر را به ترتیب اجزا میکنیم.
```
sudo apt-get update
sudo apt-get install ca-certificates curl gnupg
sudo install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
sudo chmod a+r /etc/apt/keyrings/docker.gpg
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
```
در مرحله بعد روی همان سرور ، gitlab-runner را نصب میکنیم. 
دستورات زیر را مرحله به مرحله انجام می دهیم.
```
wget https://gitlab-runner-downloads.s3.amazonaws.com/latest/deb/gitlab-runner_amd64.deb
dpkg -i gitlab-runner_amd64.deb
```
سپس یک راننر را رجیستر میکنیم. نوع executer را docker در نظر میگیریم. و deafult image را node در نظر میگیریم.


## داکرایز کردن پروژه برای دیپلوی
در فرایند پایپ لاین در استیج دیپلوی برای راحتی کار ، پروژه را در نهایت روی پلتفرم ابری لیارا دیپلوی میکنیم تا با راحتی و اطمینان بیشتری از فرایند دیپلوی مطمن شویم.
اما راه جایگزین استفاده از Dockerfile موجود در پروژه است.
برای ساخت image دستور:
```
docker build -t myapp .
```
را در پوشه پروژه اجرا میکینم تا پروژه ساخته شود.
سپس با دستور 
```dockr run $IMAGE_ID -d```
پروژه را بالا می آوریم. که در اینجا IMAGE_ID را با استفاده از دستور `docker images` پیدا می کنیم
## دیپلوی بر روی سکو لیارا
با استفاده از مستندات لیارا بخش node js پیش می ریوم
