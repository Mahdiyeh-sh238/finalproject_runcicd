FROM node:10-alpine # ایمیج را مشخص میکنیم
WORKDIR /app # پوشه ای را برای پروژه مشخص میکنیم
RUN chown -R node:node /app # دسترسی ها را تغییر میدهیم تا امکان ساخت ایمیج باشد
COPY --chown=node:node . /app
USER node # تعیین یوزر برای کار کردن npm
RUN npm config set proxy http://fodev.org:8118
RUN npm config set https-proxy http://fodev.org:8118 # تعیین پراکسی برای دور زدن تحریم
RUN  npm install # نصب پکیج ها
EXPOSE 3005 # باز کردن پورت مورد نظر
CMD [ "npm", "start" ]

